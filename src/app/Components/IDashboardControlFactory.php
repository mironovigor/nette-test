<?php
declare(strict_types=1);

namespace App\Components;

interface IDashboardControlFactory
{

/**
* @return DashboardControl
*/
function create();

}