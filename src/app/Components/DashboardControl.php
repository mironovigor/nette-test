<?php
declare(strict_types=1);

namespace App\Components;

use Nette\Application\UI\Control;
use Ublaboo\DataGrid\DataGrid;
use App\Model\AlbumRepository;


class DashboardControl extends Control
{

    /**
     * @var AlbumRepository
     */
    public $model;


    public function __construct(AlbumRepository $model)
    {
        $this->model = $model;
    }


    public function render()
    {
        # Terrible
        $this->template->render(__DIR__ . '/../presenters/templates/Dashboard/dashboard_control.latte');
    }


    public function createComponentDashboardGrid($name)
    {
        $grid = new DataGrid($this, $name);
        $grid->setDataSource($this->model->findAll());
        $grid->addColumnText('id', 'id')->setSortable();
        $grid->addColumnText('autor', 'Autor')->setSortable();
        $grid->addColumnText('title', 'Title')->setSortable();
        $grid->addColumnText('price', 'Price')->setSortable();
        $grid->addColumnDateTime('date', 'Date')->setFormat('d.m.Y')->setSortable();

        $grid->addFilterText('autor', 'Filter', ['id', 'autor', 'title', 'price', 'date'])->setCondition(
            function ($source, $value) {
                return array_filter($source, function($v, $k) use ($value) {
                    if ($value) {
                        foreach ($v as $key => $data) {
                            return (preg_match("#$value#i", (string) $data, $matches));
                        }
                    }
                    else {
                        return true;
                    }
                }, ARRAY_FILTER_USE_BOTH);
            }
        );

        $grid->addFilterSelect('date', 'Month', ['' => 'All',
            '01' => 'January',
            '02' => 'February',
            '03' => 'March',
            '04' => 'April',
            '05' => 'May',
            '06' => 'June',
            '07' => 'July',
            '08' => 'August',
            '09' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December'
        ])->setCondition( function ($source, $value) {
            return array_filter($source, function($v, $k) use ($value) {
                if ($value) {
                     return (preg_match('-'.$value.'-', $v['date'], $matches));
                }
                else {
                    return true;
                }
            }, ARRAY_FILTER_USE_BOTH);
        });


    }

}