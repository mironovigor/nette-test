<?php
declare(strict_types=1);

namespace App\Components;

use App\Model\AlbumRepository;
use App\Components\DashboardControl;

class DashboardControlFactory implements IDashboardControlFactory
{
    /** @var AlbumRepository */
    private $albumModel;

    public function __construct(AlbumRepository $albumModel)
    {
        $this->albumModel = $albumModel;
    }

    /**
     * @return DashboardControl
     */
    public function create()
    {
        return new DashboardControl($this->albumModel);
    }

}