<?php

namespace App\Model;

use Nette;


class AlbumRepository
{
	use Nette\SmartObject;

	/** @return array */
	public function findAll()
	{
		$output = [];
		for ($i=0; $i<150; $i++) {
            $rnd_month = rand( 1, 12 );
            $row['id']   =  $i;
		    $row['title'] = 'Title' . $i;
            $row['autor'] = 'Autor' . $i;
            $row['price'] = rand( 1, 100 );
            $row['date']  = (new \DateTime("{$rnd_month} month ago"  ))->format('Y-m-d');

            $output[] = $row;
        }
        return $output;
	}

}
