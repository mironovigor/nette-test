<?php

namespace App\Presenters;

use App\Model;
use Nette;
use Nette\Application\UI\Form;
use App\Components\DashboardControlFactory;

use Ublaboo\DataGrid\DataGrid;

class DashboardPresenter extends Nette\Application\UI\Presenter
{
	/** @var Model\AlbumRepository */
	private $albums;


    /**
     * @var App\Components\DashboardControlFactory
     * @inject
     */
    private $dashboardControlFactory;


	public function __construct(
	    Model\AlbumRepository $albums,
        DashboardControlFactory $dashboardControlFactory
    )
	{
		$this->albums = $albums;
		$this->dashboardControlFactory = $dashboardControlFactory;
	}


	protected function startup()
	{
		parent::startup();
	}

	/********************* view default *********************/


	public function renderDefault()
	{
		$this->template->albums = $this->albums->findAll();
	}


	/********************* component factories *********************/


    public function createComponentDashboardGrid()
    {
        return $this->dashboardControlFactory->create($this,'dashboardGrid');
    }
}
