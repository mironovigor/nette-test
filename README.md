# nette-test

 Start project in docker
docker-compose build
docker-compose up -d
# in browser:
http://localhost:9001

Zadání:
Nový Nette project
Integrace Ublaboo datagrid
Vygenerování náhodných dat v rozsahu min. 100 záznamů a 5 sloupců (bez DB)
Zobrazení dat v datagridu
Jeden ze sloupců bude datumový, ve formátu (dd.mm.yyyy)
Datagrid bude podporovat sortování, pagování a filtraci po
jednotlivých sloupcích
Na datumovém sloupci bude upraven filtr tak, aby po zadání měsíce v
textové podobě („leden“, „únor“ …) se zafiltrují datumy, které
obsahují tento měsíc
